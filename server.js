
const express = require("express");
const app = express();
const path = require("path");
const mongoose = require("mongoose");
const routineSchema = require("./src/backend/routineSchema");
const port = 8080;
const bodyParser = require("body-parser");

mongoose.connect("mongodb://localhost:27017/exercise_routine", { useNewUrlParser: true });
mongoose.Promise = Promise;
var db = mongoose.connection;

app.use(express.static(__dirname + "/build"));
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

app.get('/api/routines', (req, resp) => {
  db.collection('routine').find({}).toArray((error, routines) => {
    resp.json({ routines });
  });
});

app.get('/api/exercises', (req, resp) => {
  db.collection('exercise').find({}).toArray((error, exercises) => {
    resp.json({ exercises });
  });
});

app.use(bodyParser.json());

app.get('*', function (req, res) {
  res.sendfile(__dirname + "/build/index.html");
});

app.listen(port, () => console.log(`server is running on localhost ${port}`));