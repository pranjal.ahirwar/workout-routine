import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import registerServiceWorker from './registerServiceWorker';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import { Provider } from "react-redux";

import Registration from "./components/Registration";
import Login from "./components/Login";
import Contact from "./components/Contact";
import SignUp from "./components/Signup";
import Exercise from "./components/Exercise";
import Yoga from "./components/Yoga";
import Home from './components/Home';
import Main from './components/view/Main';
import Routine from './components/Routine';
import Pilates from './components/Pilates';
import Warmup from './components/Warmup';
import ViewExercise from './components/ViewExercise';

import store from './store/Store'

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <div>
                <Main />
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route exact path="/routines" component={Routine} />
                    <Route exact path='/registration' component={Registration} />
                    <Route exact path='/login' component={Login} />
                    <Route exact path='/signup' component={SignUp} />
                    <Route exact path='/exercise' component={Exercise} />
                    <Route exact path='/exercise/vrikshasana' component={ViewExercise} />
                    <Route exact path="/yoga" component={Yoga} />
                    <Route exact path="/contact" component={Contact} />
                    <Route exact path="/pilates" component={Pilates} />
                    <Route exact path="/warmup" component={Warmup} />
                </Switch>
            </div>
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));
registerServiceWorker();
