import React, { Component } from 'react';
import buzzer from '../sounds/buzzer.mp3';
import VoicePlayer from '../VoiceModulation/VoicePlayer';
import VoiceRecognition from '../VoiceModulation/VoiceRecognition';
import { connect } from "react-redux";
import { getData } from "../actions/action";

var status = false;
class ViewExercise extends Component {
    constructor(props) {
        super(props);
        this.buzzer = new Audio(buzzer);
        this.state = { secondsElapsed: 0 };
        // this.state = { play: false };
    }
    durationPlay = () => {
        console.log(this.refs.duration.value);
        this.intervalID = setTimeout(
            () => this.tick(),
            this.refs.duration.value
        );
    }
    tick(e) {
        this.buzzer.play();
        clearInterval(this.incrementer);
        this.state = { secondsElapsed: 0 };
    }
    getMinutes = () => {
        return (Math.floor(this.state.secondsElapsed / 60));
    }
    getSeconds = () => {
        return (('0' + this.state.secondsElapsed % 60).slice(-2));
    }
    handleStartClick = (e) => {
        let t = this;
        this.incrementer = setInterval(function () {
            t.setState({
                secondsElapsed: (t.state.secondsElapsed + 1)
            });
        }, 1000);
    }
    handleStopClick = () => {
        clearInterval(this.incrementer);
    }
    playBuzzer = () => {
        this.buzzer.play();
    }
    playSteps = () => {
        console.log('asfa')
        return (
            <VoicePlayer play
                text="Sit up comfortably on your heels. • Roll your torso forward, bringing your forehead to rest on the bed in front of you. • Lower your chest as close to your knees as you comfortably can, extending your arms in front of you. • Hold the pose and breathe."
                stop />
        );
    }
    someMethod = () => {
        this.setState({
            play: true
        })
    }
    getData = () => {
        this.props.getData();
      };
    
      render() {
        return <div>{console.log(this.props.routines)}</div>;
      }
    render() {
        return (
            <div>
                <div>{console.log(this.props.routines)}</div>;
                <div className="exercise">
                    <div className="exercise-title">
                        <h2>Vrikshasana</h2>
                    </div>
                    <div className="exercise-banner">
                        <img src={require('../images/yoga/Vrikshasana.jpg')} />
                    </div>
                    <div className="exercise-description">
                        <p className="steps-header">
                        Steps to perform exercise : 
                        </p>
                        <p className="steps-text">Sit up comfortably on your heels. • Roll your torso forward, bringing your forehead to rest on the bed in front of you. • Lower your chest as close to your knees as you comfortably can, extending your arms in front of you. • Hold the pose and breathe.
                            </p>
                        <button onClick={this.someMethod}>Steps</button>
                        {this.state.play ? this.playSteps() : console.log(this.playSteps())}
                    </div>
                    <div className="exercise-timer">
                        <h1 ref="stopWatch">{this.getMinutes()}:{this.getSeconds()}</h1>
                        <input value="6000" style={{ display: 'none' }} type="text" ref="duration" />
                        <button onClick={(event) => { this.handleStartClick(event); this.durationPlay(event) }}>
                            Start
                    </button>
                    </div>

                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    return { routines: state };
  }
  
  export default connect(
    mapStateToProps,
    { getData }
  )(ViewExercise);
  