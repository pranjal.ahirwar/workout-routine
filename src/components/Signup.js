import React, { Component } from 'react';
import banner from '../images/banner.jpg';
import Footer from './view/Footer';

export default class componentName extends Component {
    render() {
        return (
            <div>
            <div className="signup"><div className="banner">
            <img src={banner} />
            <div className="inner-banner"></div>
                <form action="submit">
                    <div className="field">
                    <input type="text" name="name" placeholder="Full Name" />
                    </div>
                    <div className="field">
                    <input type="text" name="username" placeholder="Username" />
                    </div>
                    <div className="field">
                    <input type="password" name="password" placeholder="Password" />
                    </div>
                    {/* <div className="field">
                    <input type="email" name="email" placeholder="Email" />
                    </div>
                    <div className="field">
                    <input type="text" name="mobile" placeholder="Mobile No." />
                    </div> */}
                    <div className="field-btn">
                        <button className="signup-btn">Signup</button>
                    </div>
                    <div className="field-btn">
                        <button className="signup-btn-oauth">Signup with Google</button>
                    </div>
                </form>
            </div>
            </div>
            <Footer />
            </div>
        )
    }
}
