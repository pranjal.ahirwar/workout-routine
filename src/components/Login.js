import React, { Component } from 'react';
import banner from '../images/banner.jpg';
import Footer from './view/Footer';

export default class componentName extends Component {
    render() {
        return (
            <div>
                <div className="login">
                    <div className="banner">
                        <img src={banner} />
                        <div className="inner-banner"></div>
                        <form action="submit">
                            <div className="field">
                                <input type="text" name="username" placeholder="Username" />
                            </div>
                            <div className="field">
                                <input type="password" name="password" placeholder="Password" />
                            </div>
                            <div className="field-btn">
                                <button className="login-btn">Login</button>
                            </div>
                            <div className="field-btn">
                                <button className="login-btn-oauth">Login with Google</button>
                            </div>
                        </form>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}
