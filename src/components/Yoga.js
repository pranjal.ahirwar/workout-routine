import React, { Component } from 'react'
import buzzer from '../sounds/buzzer.mp3';
import VoicePlayer from '../VoiceModulation/VoicePlayer';
import VoiceRecognition from '../VoiceModulation/VoiceRecognition';

class Yoga extends Component {
    constructor(props) {
        super(props);
        this.buzzer = new Audio(buzzer);
        this.state = { secondsElapsed: 0 }
    }
    durationPlay = () => {
        console.log(this.refs.duration.value);
        this.intervalID = setTimeout(
            () => this.tick(),
            this.refs.duration.value
        );
    }
    tick(e) {
        this.buzzer.play();
        clearInterval(this.incrementer);
        this.state = { secondsElapsed: 0 };
    }
    getMinutes = () => {
        return (Math.floor(this.state.secondsElapsed / 60));
    }
    getSeconds = () => {
        return (('0' + this.state.secondsElapsed % 60).slice(-2));
    }
    handleStartClick = (e) => {
        let t = this;
        this.incrementer = setInterval(function () {
            t.setState({
                secondsElapsed: (t.state.secondsElapsed + 1)
            });
        }, 1000);
    }
    handleStopClick = () => {
        clearInterval(this.incrementer);
    }
    playBuzzer = () => {
        this.buzzer.play();
    }
    componentDidMount() {
        this.getData()
    }
    getData() {
        // fetch("/api/routines",{
        //     headers :{
        //         "content-type" : "application/json"
        //     }
        // }).then(res => res.json())
        // .then(data =>{
        //     console.log(data);
        // })
    }
    render() {
        return (<div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/Vrikshasana.jpg')} />
                </div>

                <div>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since.
                    {/* <VoicePlayer play 
                    text="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since."
                    stop
                    /> */}
                    <div>
                        <h3>Name : data[0].routineExercises[0].exerciseName</h3>
                        <p>Steps : data[0].routineExercises[0].steps</p>
                    </div>
                </div>
                <div>
                    <h1 ref="stopWatch">{this.getMinutes()}:{this.getSeconds()}</h1>
                    <input type="text" ref="duration" />
                    <button onClick={(event) => { this.handleStartClick(event); this.durationPlay(event) }}>
                        Play
                    </button>
                </div>

            </div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/Kumbhakasana.jpg')} />
                </div>
                <div>
                    description
                </div>
                <div>
                    <button onClick={this.playBuzzer}>Start</button>
                </div>

            </div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/Virabhadrasana-1.jpg')} />
                </div>
                <div>
                    description
                </div>
                <div>
                    <button onClick={this.playBuzzer}>Start</button>
                </div>

            </div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/Virabhadrasana.jpg')} />
                </div>
                <div>
                    description
                </div>
                <div>
                    <button onClick={this.playBuzzer}>Start</button>
                </div>

            </div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/Naukasana.jpg')} />
                </div>
                <div>
                    description
                </div>
                <div>
                    <button onClick={this.playBuzzer}>Start</button>
                </div>

            </div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/Dhanurasana.jpg')} />
                </div>
                <div>
                    description
                </div>
                <div>
                    <button onClick={this.playBuzzer}>Start</button>
                </div>

            </div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/cobra.jpg')} />
                </div>
                <div>
                    description
                </div>
                <div>
                    <button onClick={this.playBuzzer}>Start</button>
                </div>

            </div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/crow_pose.jpg')} />
                </div>
                <div>
                    description
                </div>
                <div>
                    <button onClick={this.playBuzzer}>Start</button>
                </div>

            </div>
            <hr />
            <div className="yoga">
                <div>
                    <img className="yoga-image" src={require('../images/yoga/seated.jpg')} />
                </div>
                <div>
                    description
                </div>
                <div>
                    <button onClick={this.playBuzzer}>Start</button>
                </div>

            </div>
            <hr />
        </div>);
    }
}

export default Yoga;