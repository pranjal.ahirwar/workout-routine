import React, { Component } from "react";
import Yoga from "./Yoga";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

class Exercise extends Component {
  render() {
    return (
      <div className="exercise-container">
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/Vrikshasana.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/Kumbhakasana.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/Virabhadrasana-1.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/Virabhadrasana.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/Naukasana.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/Dhanurasana.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/cobra.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/crow_pose.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="yoga-image" src={require('../images/yoga/seated.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/hundred.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/Bridge Roll-Up.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/Roll Up.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/Swimming.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/Seal.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/Sliding Lunge.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/stomach massage.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/Leg Circle.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/Double Leg Stretch.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="pilates-image" src={require('../images/pilates/criss-cross.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/neck-rotation.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/high-knee-taps.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/lunge.jpeg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/squat.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/tricep-dips-onchair.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/abdominal-crunches.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/plank.jpeg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/superman.jpg')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/pushups.png')} />
          </Link>
        </div>
        <div className="exercise-content">
          <Link to="/exercise/vrikshasana">
            <img className="warmup-image" src={require('../images/warmup/donkey-kicks-butt-workout.png')} />
          </Link>
        </div>
      </div>
    );
  }
}

export default Exercise;
